package ru.veor.devcolibri;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {

    private Button button;
    private Toolbar toolbar;

    public void onShow() {
        Toast toast = Toast.makeText(getApplicationContext(), "Starting...", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolBar();
        initButton();
        onShow();
    }

    private void initButton() {
        button = (Button) findViewById(R.id.forward);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShow();
                Intent intent = new Intent(v.getContext(), LastActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        toolbar.inflateMenu(R.menu.menu);
    }

    public void onPostion(View v) {
        Context context = getApplicationContext();
        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(context, "portrait view", Toast.LENGTH_SHORT).show();
        }

        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(context, "lanscape view", Toast.LENGTH_SHORT).show();
        }
    }
}
